#!/bin/bash

#  1. Copy the drivers
 
   cp driver* ../drivers/

#  2. Copy the modules

   cp m_*  ../modules/

#  3. copy the scripts

   cp 0*              ../scripts/
   cp clean_all.sh    ../scripts/
   cp back_up.sh      ../scripts/
   cp compile_all.sh  ../scripts/

