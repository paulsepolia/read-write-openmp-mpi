#!/bin/bash

  #   1.
  sh  001_compile_matrix_write_ifort.sh ; rm *.o ; rm *.mod

  #   2.
  sh  002_compile_matrix_write_open.sh ; rm *.o ; rm *.mod

  #   3.
  sh  003_compile_matrix_write_gfortran.sh ; rm *.o ; rm *.mod

  #   4.
  sh  004_compile_matrix_read_ifort.sh ; rm *.o ; rm *.mod

  #   5.
  sh  005_compile_matrix_read_open.sh ; rm *.o ; rm *.mod

  #   6.
  sh  006_compile_matrix_read_open.sh ; rm *.o ; rm *.mod

  #   7.
  sh  007_compile_matrix_read_write_ifort.sh ; rm *.o ; rm *.mod

  #   8.
  sh  008_compile_matrix_read_write_open.sh ; rm *.o ; rm *.mod

  #   9.
  sh  009_compile_matrix_read_write_gfortran.sh ; rm *.o ; rm *.mod

  #   10.
  sh  010_compile_vector_write_ifort.sh ; rm *.o ; rm *.mod

  #   11.
  sh  011_compile_vector_write_open.sh ; rm *.o ; rm *.mod

  #   12.
  sh  012_compile_vector_write_gfortran.sh ; rm *.o ; rm *.mod

  #   13.
  sh  013_compile_vector_read_ifort.sh ; rm *.o ; rm *.mod

  #   14.
  sh  014_compile_vector_read_open.sh ; rm *.o ; rm *.mod

  #   15.
  sh  015_compile_vector_read_gfortran.sh ; rm *.o ; rm *.mod

  #   16.
  sh  016_compile_vector_read_write_ifort.sh ; rm *.o ; rm *.mod

  #   17.
  sh  017_compile_vector_read_write_open.sh ; rm *.o ; rm *.mod

  #   18.
  sh  018_compile_vector_read_write_gfortran.sh ; rm *.o ; rm *.mod

