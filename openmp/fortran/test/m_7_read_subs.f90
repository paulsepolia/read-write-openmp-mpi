!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  module m_7_read_subs

  implicit none

  contains

!================================!
! 1. Subroutine: matrix_read_sub !
!================================!

  subroutine matrix_read_sub(m,              & !  1.
                             n,              & !  2.
                             num_files,      & !  3.
                             a,              & !  4.
                             file_system,    & !  5.
                             head_file_name, & !  6.
                             out_format,     & !  7.
                             trials,         & !  8.
                             output_yes_no,  & !  9.
                             test_yes_no,    & ! 10.
                             stat_close)       ! 11.
 
  use m_1_parameters,  only: si, dr, len_a, &
                             OUTPUT_YES,    &
                             TEST_YES
  use m_2_functions,   only: matrix_function
  use m_6_read_openmp, only: matrix_read_openmp_all

  implicit none

  !  1. Interface variables

  integer(kind=si), intent(out)                           :: m              ! rows
  integer(kind=si), intent(out)                           :: n              ! columns
  integer(kind=si), intent(in)                            :: num_files      ! number of files
  real(kind=dr), allocatable, dimension(:,:), intent(out) :: a              ! matrix
  character(len=len_a), intent(in)                        :: file_system    ! "lustre_off", "lustre_on"
  character(len=len_a), intent(in)                        :: head_file_name !  head of matrix file name
  character(len=len_a), intent(in)                        :: out_format     ! "by_column", "by_row"
  integer(kind=si), intent(in)                            :: trials         ! number of reads
  character(len=len_a), intent(in)                        :: output_yes_no  ! "yes", "no"
  character(len=len_a), intent(in)                        :: test_yes_no    ! "yes", "no"
  character(len=len_a), intent(in)                        :: stat_close     ! "keep", "delete"

  !  2. Local variables and parameters

  integer(kind=si) :: i
  integer(kind=si) :: j
  integer(kind=si) :: k
  real(kind=dr)    :: t1
  real(kind=dr)    :: t2
  real(kind=dr)    :: t1_step
  real(kind=dr)    :: val_tmp
  integer(kind=si) :: count_max
  integer(kind=si) :: count_rate
  integer(kind=si) :: sys_t1
  integer(kind=si) :: sys_t2
  integer(kind=si) :: sys_t3
  integer(kind=si) :: sys_t4
  integer(kind=si) :: sys_t1_step
  real(kind=dr)    :: sys_tmp
  real(kind=dr)    :: sys_test

  !  3. Matrix OpenMP write
 
  sys_test = 0.0_dr
 
  do k = 1, trials

    !  3-1. t1
 
    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t1_step)
      call system_clock(sys_t1_step, count_rate, count_max)
      if (k == 1_si) then
        call cpu_time(t1)
        call system_clock(sys_t1, count_rate, count_max)
      end if
    end if

    !  3-2. Read the matrix 

    call matrix_read_openmp_all(m,              & !  1.
                                n,              & !  2.
                                a,              & !  3.
                                file_system,    & !  4.
                                head_file_name, & !  5.
                                out_format,     & !  6.
                                num_files,      & !  7.
                                stat_close)       !  8.

    !  3-3. Some counter to indicate the progress  

    if (output_yes_no == OUTPUT_YES) then
      write(*,*) ""
      write(*,*) " 1 --> Counter Matrix Read -------------------->", k
    end if

    !  3-4. t2 and total time

    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t2)
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1_step)*1.0_dr/count_rate
      write(*,*) ""
      write(*,*) " 2 --> Total CPU time for one read is:"
      write(*,*) "   --> ",t2-t1_step, " seconds."
      write(*,*) " 3 --> Total Wall-Clock time for one read is:"
      write(*,*) "   --> ",sys_tmp, " seconds."
      write(*,*) " 4 --> The read speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1_step)*60_si,&
                 " GBytes per CPU minute."
      write(*,*) " 5 --> The read speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1_step),&
                 " MBytes per CPU second."
      write(*,*) " 6 --> The read speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                 " GBytes per Wall-Clock minute."
      write(*,*) " 7 --> The read speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                 " MBytes per Wall-Clock second."

      write(*,*) ""
      if (k == trials) then
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp=(sys_t2-sys_t1)*1.0_dr/count_rate-sys_test*trials
        call cpu_time(t2)
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
        write(*,*) " 8 --> Total CPU time for", trials," reads is:"
        write(*,*) "   --> ",t2-t1, " seconds."
        write(*,*) " 9 --> Total Wall-Clock time for", trials," reads is:"
        write(*,*) "   --> ",sys_tmp, " seconds."
        write(*,*) "10 --> Total amount of data read is:"
        write(*,*) "   --> ",trials*8*m*real(n,kind=dr)/1024.0_dr**3.0_dr," GBytes."
        write(*,*) "11 --> The average read speed is:"
        write(*,*) "   --> ",trials*8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1)*60_si,&
                   " GBytes per CPU minute."
        write(*,*) "12 --> The average read speed is:"
        write(*,*) "   --> ",trials*8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1),&
                   " MBytes per CPU second."
        write(*,*) "13 --> The average read speed is:"
        write(*,*) "   --> ",trials*8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                   " GBytes per Wall-Clock minute."
        write(*,*) "14 --> The average read speed is:"
        write(*,*) "   --> ",trials*8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                   " MBytes per Wall-Clock second."
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
      end if
    end if

    !  3-5. Testing

    call system_clock(sys_t3, count_rate, count_max)
    if (test_yes_no == TEST_YES) then
      if (output_yes_no == OUTPUT_YES ) then
        write(*,*) "XX --> Testing."
      end if
      do i = 1, n
        do j = 1, m
          val_tmp = matrix_function(j,i) - a(j,i)
          if (val_tmp /= 0.0_dr) then
            write(*,*) "XX --> Error at: ", j, ", ", i, " diff = ", val_tmp
            write(*,*) "XX --> Stop"
            stop
          end if
        end do
      end do
      if (output_yes_no == OUTPUT_YES ) then
        write(*,*) "XX --> Success."
        write(*,*) ""
      end if
    end if
    call system_clock(sys_t4, count_rate, count_max)
    sys_test=(sys_t4-sys_t3)*1.0_dr/count_rate
 
   ! 3-6. Local deallocation

    if (k /= trials) then
      deallocate(a)
    end if

  end do
 
  end subroutine matrix_read_sub

!================================!
! 2. Subroutine: vector_read_sub !
!================================!

  subroutine vector_read_sub(n,              & !  1.
                             num_files,      & !  2.
                             w,              & !  3.
                             file_system,    & !  4.
                             head_file_name, & !  5.
                             trials,         & !  6.
                             output_yes_no,  & !  7.
                             test_yes_no,    & !  8.
                             stat_close)       !  9.

  use m_1_parameters,  only: si, di, dr, len_a, &
                             OUTPUT_YES,        &
                             TEST_YES
  use m_2_functions,   only: vector_function
  use m_6_read_openmp, only: vector_read_openmp_all

  implicit none

  !  1. Interface variables

  integer(kind=di), intent(out)                         :: n              ! rows
  integer(kind=si), intent(in)                          :: num_files      ! number of files
  real(kind=dr), allocatable, dimension(:), intent(out) :: w              ! vector
  character(len=len_a), intent(in)                      :: file_system    ! "lustre_off", "lustre_on"
  character(len=len_a), intent(in)                      :: head_file_name !  head of vector file name
  integer(kind=si), intent(in)                          :: trials         ! number of reads
  character(len=len_a), intent(in)                      :: output_yes_no  ! "yes", "no"
  character(len=len_a), intent(in)                      :: test_yes_no    ! "yes", "no"
  character(len=len_a), intent(in)                      :: stat_close     ! "keep", "delete"

  !  2. Local variables and parameters

  integer(kind=di) :: i
  integer(kind=si) :: k
  real(kind=dr)    :: t1
  real(kind=dr)    :: t1_step
  real(kind=dr)    :: t2
  real(kind=dr)    :: val_tmp
  integer(kind=si) :: count_max
  integer(kind=si) :: count_rate
  integer(kind=si) :: sys_t1
  integer(kind=si) :: sys_t2
  integer(kind=si) :: sys_t3
  integer(kind=si) :: sys_t4
  integer(kind=si) :: sys_t1_step
  real(kind=dr)    :: sys_tmp
  real(kind=dr)    :: sys_test

  !  3. Vector OpenMP write
 
  sys_test = 0.0_dr
 
  do k = 1, trials

    !  3-1. t1
 
    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t1_step)
      call system_clock(sys_t1_step, count_rate, count_max)
      if (k == 1_si) then
        call cpu_time(t1)
        call system_clock(sys_t1, count_rate, count_max)
      end if
    end if

    !  3-2. Reading 

    call vector_read_openmp_all(n,              & !  1.
                                w,              & !  2.
                                file_system,    & !  3.
                                head_file_name, & !  4.
                                num_files,      & !  5.
                                stat_close)       !  6.

    !  3-3. Some counter to indicate the progress  

    if (output_yes_no == OUTPUT_YES) then
      write(*,*) ""
      write(*,*) " 1 --> Counter Vector Read -------------------->", k
    end if

    !  3-4. t2 and total time

    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t2)
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1_step)*1.0_dr/count_rate
      write(*,*) ""
      write(*,*) " 2 --> Total CPU time for one read is:"
      write(*,*) "   --> ",t2-t1_step, " seconds."
      write(*,*) " 3 --> Total Wall-Clock time for one read is:"
      write(*,*) "   --> ",sys_tmp, " seconds."
      write(*,*) " 4 --> The read speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1_step)*60_si,&
                 " GBytes per CPU minute."
      write(*,*) " 5 --> The read speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1_step),&
                 " MBytes per CPU second."
      write(*,*) " 6 --> The read speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                 " GBytes per Wall-Clock minute."
      write(*,*) " 7 --> The read speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                 " MBytes per Wall-Clock second."

      write(*,*) ""
      if (k == trials) then
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1)*1.0_dr/count_rate-sys_test*trials
        call cpu_time(t2)
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
        write(*,*) " 8 --> Total CPU time for", trials," reads is:"
        write(*,*) "   --> ",t2-t1, " seconds."
        write(*,*) " 9 --> Total Wall-Clock time for", trials," reads is:"
        write(*,*) "   --> ",sys_tmp, " seconds."
        write(*,*) "10 --> Total amount of data read is:"
        write(*,*) "   --> ",trials*8*real(n,kind=dr)/1024.0_dr**3.0_dr," GBytes."
        write(*,*) "11 --> The average read speed is:"
        write(*,*) "   --> ",trials*8*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1)*60_si,&
                   " GBytes per CPU minute."
        write(*,*) "12 --> The average read speed is:"
        write(*,*) "   --> ",trials*8*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1),&
                   " MBytes per CPU second."
        write(*,*) "13 --> The average read speed is:"
        write(*,*) "   --> ",trials*8*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                   " GBytes per Wall-Clock minute."
        write(*,*) "14 --> The average read speed is:"
        write(*,*) "   --> ",trials*8*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                   " MBytes per Wall-Clock second."
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
      end if
    end if

    !  3-5. Testing

    call system_clock(sys_t3, count_rate, count_max)
    if (test_yes_no == TEST_YES) then
      if (output_yes_no == OUTPUT_YES ) then
        write(*,*) "XX --> Testing."
      end if
      do i = 1, n
        val_tmp = vector_function(i) - w(i)
        if (val_tmp /= 0.0_dr) then
          write(*,*) "XX --> Error at: ",i,", diff = ", val_tmp
          write(*,*) "XX --> Stop"
          stop
        end if
      end do
      if (output_yes_no == OUTPUT_YES ) then
        write(*,*) "XX --> Success."
        write(*,*) ""
      end if
    end if
    call system_clock(sys_t4, count_rate, count_max)
    sys_test=(sys_t4-sys_t3)*1.0_dr/count_rate
 
   ! 3-6. Local deallocation

    if (k /= trials) then
      deallocate(w)
    end if

  end do
 
  end subroutine vector_read_sub

  end module m_7_read_subs

!======!
! FINI !
!======!
