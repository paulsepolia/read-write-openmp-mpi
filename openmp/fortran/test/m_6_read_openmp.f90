!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  module m_6_read_openmp

  implicit none

  contains

!===========================!
! 1. matrix_read_openmp_all !
!===========================!

  subroutine matrix_read_openmp_all(m,           & !  1.
                                    n,           & !  2.
                                    a,           & !  3.
                                    file_system, & !  4.
                                    matrix_file, & !  5.
                                    out_format,  & !  6.
                                    num_files,   & !  7.
                                    stat_close)    !  8.

  use m_1_parameters, only: si, di, dr, len_a, &
                            FILE_SYS_LU_ON,    &
                            FILE_SYS_LU_OFF,   &
                            OUT_FORMAT_COL,    &
                            OUT_FORMAT_ROW

  implicit none

  !  1. Interface variables

  integer(kind=si), intent(out)                             :: m
  integer(kind=si), intent(out)                             :: n
  real(kind=dr), allocatable, dimension(:,:), intent(inout) :: a
  character(len=len_a), intent(in)                          :: file_system
  character(len=len_a), intent(in)                          :: matrix_file
  character(len=len_a), intent(in)                          :: out_format
  integer(kind=si), intent(in)                              :: num_files
  character(len=len_a), intent(in)                          :: stat_close

  !  2. Local variables and parameters

  integer(kind=si)                            :: i
  integer(kind=si)                            :: j
  integer(kind=si)                            :: chunk
  integer(kind=si)                            :: low_ba
  integer(kind=si)                            :: up_ba
  integer(kind=si)                            :: i_am
  integer(kind=si)                            :: n_threads
  integer(kind=si)                            :: nt
  integer(kind=si)                            :: rec_len
  integer(kind=si)                            :: omp_get_num_threads
  integer(kind=si)                            :: omp_get_thread_num
  character(len=len_a)                        :: file_id
  real(kind=dr)                               :: dimen_line
  integer(kind=si), parameter                 :: basic_unit = 18_si
  character(len=len_a), parameter             :: fmt_a      = "(I10)"
  real(kind=dr)                               :: m_tmp
  real(kind=dr)                               :: n_tmp
  real(kind=dr)                               :: low_ba_tmp
  real(kind=dr)                               :: up_ba_tmp
  integer(kind=si), allocatable, dimension(:) :: low_ba_array
  integer(kind=si), allocatable, dimension(:) :: up_ba_array
  real(kind=dr)                               :: maxval

  !  3. Local allocations

  allocate(low_ba_array(1:num_files))
  allocate(up_ba_array(1:num_files))

  !  4. Write the eigenvectors

  !=============!
  ! case: ALPHA !
  !=============!

  ! num_files = 1_si

  if (num_files == 1_si) then

    !  4-1. Set the common record length

    dimen_line = 0.123456789_dr
    inquire(iolength = rec_len) dimen_line

    !  4-2. Open the unit to write

    open(unit=basic_unit,        &
         form="unformatted",     &
         file=trim(matrix_file), &
         status="old",           &
         action="read",          &
         access="direct",        &
         recl=rec_len)

    !  4-3. Write the dimensions

    read(unit=basic_unit, rec=2_si) m_tmp ! rows
    read(unit=basic_unit, rec=4_si) n_tmp ! columns

    m = int(m_tmp, kind=si)
    n = int(n_tmp, kind=si)

    !  4-4. Allocate the matrix
 
    allocate(a(1:m,1:n))
 
    !  4-5. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  4-6. Read now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel           &
    !$omp default(none)      &
    !$omp private(i)         &
    !$omp private(j)         &
    !$omp private(chunk)     &
    !$omp private(i_am)      &
    !$omp private(low_ba)    &
    !$omp private(up_ba)     &
    !$omp shared(n)          &
    !$omp shared(m)          &
    !$omp shared(a)          &
    !$omp shared(n_threads)  &
    !$omp shared(out_format) &
    !$omp shared(nt)         &
    !$omp num_threads(nt)

    ! a.

    i_am = omp_get_thread_num()
    n_threads = omp_get_num_threads()
    chunk = int(n, kind=si) / n_threads
    low_ba = i_am * chunk + 1_si
    up_ba = (i_am + 1_si) * chunk

    ! b.

    if (i_am == n_threads-1) then
      up_ba = int(n, kind=si)
    end if

    ! c. out_format: "by_column" or "by_row"
  
    if (out_format == OUT_FORMAT_COL) then

      do i = low_ba, up_ba
        do j = 1, int(m, kind=si)

          read(unit=basic_unit, rec=4_di+j+(i-1)*int(n, kind=di)) a(j,i)

        end do
      end do

    else if (out_format == OUT_FORMAT_ROW) then

      do i = low_ba, up_ba
        do j = 1, int(m, kind=si)

          read(unit=basic_unit, rec=4_di+i+(j-1)*int(n, kind=di)) a(j,i)

        end do
      end do

    end if

    !$omp end parallel

    !  4-7. Close the unit and keep the file

    close(unit=basic_unit, status=stat_close)

  end if

  !====================!
  ! End of case: ALPHA !
  !====================!

  !  5. Write the eigenvectors

  !============!
  ! case: BETA !
  !============!

  ! num_files > 1_si

  if (num_files > 1_si) then

    !  5-1. Set the common record length

    dimen_line = 0.123456789_dr
    inquire(iolength = rec_len) dimen_line

    !  5-2. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  5-3. Reading the dimensions of the files in serial only

    up_ba = 0_si

    if (out_format == OUT_FORMAT_COL) then

      do i = 1, num_files
 
        write(file_id, fmt_a) i-1
        file_id = adjustl(file_id)
        file_id = trim(file_id)

        open(unit=basic_unit+(i-1),           &
             form="unformatted",              &
             file=trim(matrix_file)//file_id, &
             status="old",                    &
             action="read",                   &
             access="direct",                 &
             recl=rec_len)

        read(unit=basic_unit+(i-1), rec=2_si) m_tmp      ! rows
        read(unit=basic_unit+(i-1), rec=3_si) low_ba_tmp ! columns: low_ba
        read(unit=basic_unit+(i-1), rec=4_si) up_ba_tmp  ! columns: up_ba

        up_ba_array(i)  = int(up_ba_tmp, kind=si)
        low_ba_array(i) = int(low_ba_tmp, kind=si)
        n = maxval(up_ba_array)
        m = int(m_tmp, kind=si)

        close(unit=basic_unit+(i-1), status="keep")

      end do

    else if (out_format == OUT_FORMAT_ROW) then

      do i = 1, num_files
 
        write(file_id, fmt_a) i-1
        file_id = adjustl(file_id)
        file_id = trim(file_id)

        open(unit=basic_unit+(i-1),           &
             form="unformatted",              &
             file=trim(matrix_file)//file_id, &
             status="old",                    &
             action="read",                   &
             access="direct",                 &
             recl=rec_len)

        read(unit=basic_unit+(i-1), rec=1_si) low_ba_tmp ! rows: low_ba 
        read(unit=basic_unit+(i-1), rec=2_si) up_ba_tmp  ! rows: up_ba
        read(unit=basic_unit+(i-1), rec=4_si) n_tmp      ! columns

        up_ba_array(i)  = int(up_ba_tmp, kind=si)
        low_ba_array(i) = int(low_ba_tmp, kind=si)
        m = maxval(up_ba_array)
        n = int(n_tmp, kind=si)

        close(unit=basic_unit+(i-1), status="keep")

      end do

    end if

    allocate(a(1:m,1:n))

    !  5-4. Read now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel             &
    !$omp default(none)        &
    !$omp private(i)           &
    !$omp private(j)           &
    !$omp private(chunk)       &
    !$omp private(i_am)        &
    !$omp private(low_ba)      &
    !$omp private(up_ba)       &
    !$omp shared(low_ba_array) &
    !$omp shared(up_ba_array)  &
    !$omp shared(n)            &
    !$omp shared(m)            &
    !$omp shared(a)            &
    !$omp shared(n_threads)    &
    !$omp shared(out_format)   &
    !$omp shared(nt)           &
    !$omp num_threads(nt)      &
    !$omp private(file_id)     & 
    !$omp shared(matrix_file)  &
    !$omp shared(rec_len)      &
    !$omp shared(stat_close)

    ! a.

    i_am = omp_get_thread_num()
    write(file_id, fmt_a) i_am
    file_id = adjustl(file_id)
    file_id = trim(file_id)

    open(unit=basic_unit+i_am,            &
         form="unformatted",              &
         file=trim(matrix_file)//file_id, &
         status="old",                    &
         action="read",                   &
         access="direct",                 &
         recl=rec_len)

    ! b. out_format: "by_column" or "by_row"
  
    if (out_format == OUT_FORMAT_COL) then
      
      ! b-1.

      n_threads = omp_get_num_threads()
      chunk = int(n, kind=si) / n_threads

      ! b-2.

      low_ba = low_ba_array(i_am+1)
      up_ba  = up_ba_array(i_am+1)

      do i = low_ba, up_ba
        do j = 1, int(m, kind=si)

          read(unit=basic_unit+i_am, rec=4_di+j+(i-1)*int(n, kind=di)- &
                               i_am*chunk*int(n, kind=di)) a(j,i)

        end do
      end do

    else if (out_format == OUT_FORMAT_ROW) then

      ! b-3.

      n_threads = omp_get_num_threads()
      chunk = int(m, kind=si) / n_threads

      ! b-4.

      low_ba = low_ba_array(i_am+1)
      up_ba  = up_ba_array(i_am+1)

      do j = low_ba, up_ba
        do i = 1, int(n, kind=si)

          read(unit=basic_unit+i_am, rec=4_di+i+(j-1)*int(n, kind=di)- &
                               i_am*chunk*int(n, kind=di)) a(j,i)

        end do
      end do

    end if

    close(unit=basic_unit+i_am, status=stat_close)
 
    !$omp end parallel

  end if

  !===================!
  ! End of case: BETA !
  !===================!

  end subroutine matrix_read_openmp_all

!===========================!
! 3. vector_read_openmp_all !
!===========================!

  subroutine vector_read_openmp_all(n,           & !  1.
                                    w,           & !  2.
                                    file_system, & !  3.
                                    vector_file, & !  4.
                                    num_files,   & !  5.
                                    stat_close)    !  6.

  use m_1_parameters, only: si, di, dr, len_a, &
                            FILE_SYS_LU_ON,    &
                            FILE_SYS_LU_OFF

  implicit none

  !  1. Interface variables

  integer(kind=di), intent(out)                           :: n
  real(kind=dr), allocatable, dimension(:), intent(inout) :: w
  character(len=len_a), intent(in)                        :: file_system
  character(len=len_a), intent(in)                        :: vector_file
  integer(kind=si), intent(in)                            :: num_files
  character(len=len_a), intent(in)                        :: stat_close

  !  2. Local variables and parameters

  integer(kind=di)                            :: i
  integer(kind=di)                            :: chunk
  integer(kind=di)                            :: low_ba
  integer(kind=di)                            :: up_ba
  integer(kind=si)                            :: i_am
  integer(kind=si)                            :: n_threads
  integer(kind=si)                            :: nt
  integer(kind=si)                            :: rec_len
  integer(kind=si)                            :: omp_get_num_threads
  integer(kind=si)                            :: omp_get_thread_num
  character(len=len_a)                        :: file_id
  real(kind=dr)                               :: dimen_line
  integer(kind=si), parameter                 :: basic_unit = 18_si
  character(len=len_a), parameter             :: fmt_a      = "(I10)"
  real(kind=dr)                               :: n_tmp
  real(kind=dr)                               :: low_ba_tmp
  real(kind=dr)                               :: up_ba_tmp
  integer(kind=di), allocatable, dimension(:) :: low_ba_array
  integer(kind=di), allocatable, dimension(:) :: up_ba_array
  real(kind=dr)                               :: maxval

  !  3. Local allocations

  allocate(low_ba_array(1:num_files))
  allocate(up_ba_array(1:num_files))

  !  4. Write the eigenvectors

  !=============!
  ! case: ALPHA !
  !=============!

  ! num_files = 1_si

  if (num_files == 1_si) then

    !  4-1. Set the common record length

    dimen_line = 0.123456789_dr
    inquire(iolength = rec_len) dimen_line

    !  4-2. Open the unit to write

    open(unit=basic_unit,        &
         form="unformatted",     &
         file=trim(vector_file), &
         status="old",           &
         action="read",          &
         access="direct",        &
         recl=rec_len)

    !  4-3. Write the dimensions

    read(unit=basic_unit, rec=2_si) n_tmp ! rows

    n = int(n_tmp, kind=di)

    !  4-4. Allocate the matrix
 
    allocate(w(1:n))
 
    !  4-5. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  4-6. Read now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel          &
    !$omp default(none)     &
    !$omp private(i)        &
    !$omp private(chunk)    &
    !$omp private(i_am)     &
    !$omp private(low_ba)   &
    !$omp private(up_ba)    &
    !$omp shared(n)         &
    !$omp shared(w)         &
    !$omp shared(n_threads) &
    !$omp shared(nt)        &
    !$omp num_threads(nt)

    ! a.

    i_am = omp_get_thread_num()
    n_threads = omp_get_num_threads()
    chunk = int(n, kind=di) / n_threads
    low_ba = i_am * chunk + 1_di
    up_ba = (i_am + 1_di) * chunk

    ! b.

    if (i_am == n_threads-1) then
      up_ba = int(n, kind=di)
    end if

    ! c.
  
    do i = low_ba, up_ba

      read(unit=basic_unit, rec=2_di+i) w(i)

    end do

    !$omp end parallel

    !  4-7. Close the unit and keep the file

    close(unit=basic_unit, status=stat_close)

  end if

  !====================!
  ! End of case: ALPHA !
  !====================!

  !  5. Read the vector

  !============!
  ! case: BETA !
  !============!

  ! num_files > 1_si

  if (num_files > 1_si) then

    !  5-1. Set the common record length

    dimen_line = 0.123456789_dr
    inquire(iolength = rec_len) dimen_line

    !  5-2. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  5-3. Reading the dimensions of the files in serial only

    up_ba = 0_di

    do i = 1, num_files
 
      write(file_id, fmt_a) i-1
      file_id = adjustl(file_id)
      file_id = trim(file_id)

      open(unit=basic_unit+(i-1),           &
           form="unformatted",              &
           file=trim(vector_file)//file_id, &
           status="old",                    &
           action="read",                   &
           access="direct",                 &
           recl=rec_len)

      read(unit=basic_unit+(i-1), rec=1_si) low_ba_tmp ! columns: low_ba
      read(unit=basic_unit+(i-1), rec=2_si) up_ba_tmp  ! columns: up_ba

      up_ba_array(i)  = int(up_ba_tmp, kind=di)
      low_ba_array(i) = int(low_ba_tmp, kind=di)
      n = maxval(up_ba_array)

      close(unit=basic_unit+(i-1), status="keep")

    end do

    allocate(w(1:n))

    !  5-4. Read now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel             &
    !$omp default(none)        &
    !$omp private(i)           &
    !$omp private(chunk)       &
    !$omp private(i_am)        &
    !$omp private(low_ba)      &
    !$omp private(up_ba)       &
    !$omp shared(low_ba_array) &
    !$omp shared(up_ba_array)  &
    !$omp shared(n)            &
    !$omp shared(w)            &
    !$omp shared(n_threads)    &
    !$omp shared(nt)           &
    !$omp num_threads(nt)      &
    !$omp private(file_id)     &
    !$omp shared(vector_file)  &
    !$omp shared(rec_len)      &
    !$omp shared(stat_close)

    ! a.

    i_am = omp_get_thread_num()
    write(file_id, fmt_a) i_am
    file_id = adjustl(file_id)
    file_id = trim(file_id)

    open(unit=basic_unit+i_am,            &
         form="unformatted",              &
         file=trim(vector_file)//file_id, &
         status="old",                    &
         action="read",                   &
         access="direct",                 &
         recl=rec_len)

    ! b-1.

    n_threads = omp_get_num_threads()
    chunk = int(n, kind=si) / n_threads

    ! b-2.

    low_ba = low_ba_array(i_am+1)
    up_ba  = up_ba_array(i_am+1)

    do i = low_ba, up_ba

      read(unit=basic_unit+i_am, rec=2_di+i-i_am*chunk) w(i)

    end do

    ! b-3.

    close(unit=basic_unit+i_am, status=stat_close)
 
    !$omp end parallel

  end if

  !===================!
  ! End of case: BETA !
  !===================!

  end subroutine vector_read_openmp_all

  end module m_6_read_openmp

!======!
! FINI !
!======!
