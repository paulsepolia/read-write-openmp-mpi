!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  module m_3_build_via_function

  implicit none

  contains

!====================================!
! 1. Subroutine: matrix_via_function !
!====================================!

  subroutine matrix_via_function(m,               &
                                 n,               &
                                 a,               &
                                 matrix_function)

  use m_1_parameters, only: si, dr

  implicit none

  !  1. Interface variables

  integer(kind=si), intent(in)                              :: m
  integer(kind=si), intent(in)                              :: n
  real(kind=dr), allocatable, dimension(:,:), intent(inout) :: a
  real(kind=dr), external                                   :: matrix_function

  !  2. Local variables

  integer(kind=si) :: i
  integer(kind=si) :: j
  integer(kind=si) :: chunk
  integer(kind=si) :: low_ba
  integer(kind=si) :: up_ba
  integer(kind=si) :: i_am
  integer(kind=si) :: n_threads
  integer(kind=si) :: omp_get_num_threads
  integer(kind=si) :: omp_get_thread_num

  !  3. Building a test matrix. OPTIMAL USE OF CACHE.

  !$omp parallel          &
  !$omp private(i)        &
  !$omp private(j)        &
  !$omp private(chunk)    &
  !$omp private(i_am)     &
  !$omp private(low_ba)   &
  !$omp private(up_ba)    &
  !$omp shared(m)         &
  !$omp shared(n)         &
  !$omp shared(a)         &
  !$omp shared(n_threads)

  i_am = omp_get_thread_num()
  n_threads = omp_get_num_threads()
  chunk = int(n, kind=si) / n_threads
  low_ba = i_am * chunk + 1_si
  up_ba = (i_am + 1_si) * chunk
 
  if (i_am == n_threads-1_si) then
    up_ba = int(n, kind=si)
  end if

  do i = low_ba, up_ba
    do j = 1_si, int(m, kind=si)

      a(j,i) = matrix_function(j,i)

    end do
  end do

  !$omp end parallel

  end subroutine matrix_via_function

!====================================!
! 2. Subroutine: vector_via_function !
!====================================!

  subroutine vector_via_function(n,               &
                                 w,               &
                                 vector_function)

  use m_1_parameters, only: si, di, dr

  implicit none

  !  1. Interface variables

  integer(kind=di), intent(in)                            :: n
  real(kind=dr), allocatable, dimension(:), intent(inout) :: w
  real(kind=dr), external                                 :: vector_function

  !  2. Local variables

  integer(kind=di) :: i
  integer(kind=di) :: chunk
  integer(kind=di) :: low_ba
  integer(kind=di) :: up_ba
  integer(kind=si) :: i_am
  integer(kind=si) :: n_threads
  integer(kind=si) :: omp_get_num_threads
  integer(kind=si) :: omp_get_thread_num

  !  3. Building a test matrix. OPTIMAL USE OF CACHE.

  !$omp parallel          &
  !$omp private(i)        &
  !$omp private(chunk)    &
  !$omp private(i_am)     &
  !$omp private(low_ba)   &
  !$omp private(up_ba)    &
  !$omp shared(n)         &
  !$omp shared(w)         &
  !$omp shared(n_threads)

  i_am = omp_get_thread_num()
  n_threads = omp_get_num_threads()
  chunk = int(n, kind=di) / n_threads
  low_ba = i_am * chunk + 1_di
  up_ba = (i_am + 1_di) * chunk
 
  if (i_am == n_threads-1_si) then
    up_ba = int(n, kind=di)
  end if

  do i = low_ba, up_ba

    w(i) = vector_function(i)

  end do

  !$omp end parallel

  end subroutine vector_via_function

  end module m_3_build_via_function

!======!
! FINI !
!======!
