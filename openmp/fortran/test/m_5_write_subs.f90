!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  module m_5_write_subs

  implicit none

  contains

!=================================!
! 1. Subroutine: matrix_write_sub !
!=================================!

  subroutine matrix_write_sub(m,               &
                              n,               &
                              a,               &
                              matrix_function, &
                              file_system,     &
                              file_name,       &
                              type_of_build,   &
                              out_format,      &
                              num_files,       &
                              trials,          &
                              output_yes_no,   &
                              stat_open,       &
                              stat_close)

  use m_1_parameters,         only: si, dr, len_a,          &
                                    TYPE_OF_BUILD_MATRIX,   &
                                    TYPE_OF_BUILD_FUNCTION, &
                                    OUTPUT_YES
  use m_3_build_via_function, only: matrix_via_function
  use m_4_write_openmp,       only: matrix_write_openmp_all,    &
                                    matrix_write_openmp_by_elem

  implicit none

  !  1. Interface variables 

  integer(kind=si), intent(in)                              :: m
  integer(kind=si), intent(in)                              :: n
  real(kind=dr), allocatable, dimension(:,:), intent(inout) :: a       
  real(kind=dr), external                                   :: matrix_function
  character(len=len_a), intent(in)                          :: file_system
  character(len=len_a), intent(in)                          :: file_name
  character(len=len_a), intent(in)                          :: type_of_build
  character(len=len_a), intent(in)                          :: out_format
  character(len=len_a), intent(in)                          :: num_files
  integer(kind=si), intent(in)                              :: trials
  character(len=len_a), intent(in)                          :: output_yes_no
  character(len=len_a), intent(in)                          :: stat_open
  character(len=len_a), intent(in)                          :: stat_close

  !  2. Local variables and parameters

  integer(kind=si)            :: trials_a
  integer(kind=si)            :: trials_b
  integer(kind=si)            :: i
  real(kind=dr)               :: t1
  real(kind=dr)               :: t2
  real(kind=dr)               :: t1_step
  integer(kind=si)            :: sys_t1
  integer(kind=si)            :: sys_t2
  integer(kind=si)            :: sys_t1_step
  integer(kind=si)            :: count_rate
  integer(kind=si)            :: count_max
  integer(kind=si), parameter :: unit_a = 500_si
  real(kind=dr)               :: sys_tmp

  !  3. Choose way to build the matrix 

  if (type_of_build == TYPE_OF_BUILD_FUNCTION) then
  
    trials_b = trials
    trials_a = 0_si
     
  else if (type_of_build == TYPE_OF_BUILD_MATRIX) then

    trials_b = 0_si
    trials_a = trials
  
  end if

  !  5. Write the matrix - load the matrix in RAM

  do i = 1, trials_a

    !  5-1. t1
    
    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t1_step)
      call system_clock(sys_t1_step, count_rate, count_max)
      if (i == 1_si) then
        call cpu_time(t1)
        call system_clock(sys_t1, count_rate, count_max)
      end if
    end if

    !  5-2. Allocate the matrix

    allocate(a(1:m,1:n))

    !  5-3. Build the matrix

    call matrix_via_function(m,               & !  1.
                             n,               & !  2.
                             a,               & !  3.
                             matrix_function)   !  4.

    !  5-4. Write the matrix

    call matrix_write_openmp_all(m,           & !  1.
                                 n,           & !  2.
                                 a,           & !  3.
                                 file_system, & !  4.
                                 file_name,   & !  5.
                                 out_format,  & !  6.
                                 num_files,   & !  7.
                                 stat_open,   & !  8.
                                 stat_close)    !  9.

    !  5-5. Deallocate the matrix

    deallocate(a)

    !  5-6. Some counter to indicate the progress  

    if (output_yes_no == OUTPUT_YES) then
      write(*,*) ""
      write(*,*) " 1 --> Counter Matrix Write -------------------->", i
    end if

    !  5-7. t2 and total time

    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t2)
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1_step)*1.0_dr/count_rate
      write(*,*) ""
      write(*,*) " 2 --> Total CPU time for one write is:"
      write(*,*) "   --> ",t2-t1_step, " seconds."
      write(*,*) " 3 --> Total Wall-Clock time for one write is:"
      write(*,*) "   --> ",sys_tmp, " seconds."
      write(*,*) " 4 --> The write speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1_step)*60_si,&
                 " GBytes per CPU minute."
      write(*,*) " 5 --> The write speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1_step),&
                 " MBytes per CPU second."
      write(*,*) " 6 --> The write speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                 " GBytes per Wall-Clock minute."
      write(*,*) " 7 --> The write speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                 " MBytes per Wall-Clock second."

      write(*,*) ""
      if (i == trials_a) then
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1)*1.0_dr/count_rate
        call cpu_time(t2)
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
        write(*,*) " 8 --> Total CPU time for", trials_a," writes is:"
        write(*,*) "   --> ",t2-t1, " seconds."
        write(*,*) " 9 --> Total Wall-Clock time for", trials_a," writes is:"
        write(*,*) "   --> ",sys_tmp, " seconds."
        write(*,*) "10 --> Total amount of data written is:"
        write(*,*) "   --> ",trials_a*8*m*real(n,kind=dr)/1024.0_dr**3.0_dr," GBytes."
        write(*,*) "11 --> The average write speed is:"
        write(*,*) "   --> ",trials_a*8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1)*60_si,&
                   " GBytes per CPU minute."
        write(*,*) "12 --> The average write speed is:"
        write(*,*) "   --> ",trials_a*8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1),&
                   " MBytes per CPU second."
        write(*,*) "13 --> The average write speed is:"
        write(*,*) "   --> ",trials_a*8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                   " GBytes per Wall-Clock minute."
        write(*,*) "14 --> The average write speed is:"
        write(*,*) "   --> ",trials_a*8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                   " MBytes per Wall-Clock second."
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
      end if
    end if

  end do

  !  6. Write the matrix - create element by element.

  do i = 1, trials_b

    !  6-1. t1
  
    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t1_step)
      call system_clock(sys_t1_step, count_rate, count_max)
      if (i == 1_si) then
        call cpu_time(t1)
        call system_clock(sys_t1, count_rate, count_max)
      end if
    end if

    !  6-2. Build and write the matrix

    call matrix_write_openmp_by_elem(m,               & !  1.
                                     n,               & !  2.
                                     matrix_function, & !  3.
                                     file_system,     & !  4.
                                     file_name,       & !  5.
                                     out_format,      & !  6.
                                     num_files,       & !  7.
                                     stat_open,       & !  8.
                                     stat_close)        !  9.

    !  6-3. Some counter to indicate the progress

    if (output_yes_no == OUTPUT_YES) then
      write(*,*) ""
      write(*,*) " 1 --> Counter Matrix Write -------------------->", i
    end if

    !  6-4. t2 and total time

    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t2)
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1_step)*1.0_dr/count_rate
      write(*,*) ""
      write(*,*) " 2 --> Total CPU time for one write is:"
      write(*,*) "   --> ",t2-t1_step, " seconds."
      write(*,*) " 3 --> Total Wall-Clock time for one write is:"
      write(*,*) "   --> ",sys_tmp, " seconds."
      write(*,*) " 4 --> The write speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1_step)*60_si,&
                 " GBytes per CPU minute."
      write(*,*) " 5 --> The write speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1_step),&
                 " MBytes per CPU second."
      write(*,*) " 6 --> The write speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                 " GBytes per Wall-Clock minute."
      write(*,*) " 7 --> The write speed is:"
      write(*,*) "   --> ",8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                 " MBytes per Wall-Clock second."

      write(*,*) ""
      if (i == trials_b) then
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1)*1.0_dr/count_rate
        call cpu_time(t2)
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
        write(*,*) " 8 --> Total CPU time for", trials_b," writes is:"
        write(*,*) "   --> ",t2-t1, " seconds."
        write(*,*) " 9 --> Total Wall-Clock time for", trials_b," writes is:"
        write(*,*) "   --> ",sys_tmp, " seconds."
        write(*,*) "10 --> Total amount of data written is:"
        write(*,*) "   --> ",trials_b*8*m*real(n,kind=dr)/1024.0_dr**3.0_dr," GBytes."
        write(*,*) "11 --> The average write speed is:"
        write(*,*) "   --> ",trials_b*8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1)*60_si,&
                   " GBytes per CPU minute."
        write(*,*) "12 --> The average write speed is:"
        write(*,*) "   --> ",trials_b*8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1),&
                   " MBytes per CPU second."
        write(*,*) "13 --> The average write speed is:"
        write(*,*) "   --> ",trials_b*8*m*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                   " GBytes per Wall-Clock minute."
        write(*,*) "14 --> The average write speed is:"
        write(*,*) "   --> ",trials_b*8*m*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                   " MBytes per Wall-Clock second."
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
      end if
    end if

  end do

  end subroutine matrix_write_sub

!=================================!
! 2. Subroutine: vector_write_sub !
!=================================!

  subroutine vector_write_sub(n,               &
                              w,               &
                              vector_function, &
                              file_system,     &
                              file_name,       &
                              type_of_build,   &
                              num_files,       &
                              trials,          &
                              output_yes_no,   &
                              stat_open,       &
                              stat_close)

  use m_1_parameters,         only: si, di, dr, len_a,      &
                                    TYPE_OF_BUILD_VECTOR,   &
                                    TYPE_OF_BUILD_FUNCTION, &
                                    OUTPUT_YES
  use m_3_build_via_function, only: vector_via_function
  use m_4_write_openmp,       only: vector_write_openmp_all, &
                                    vector_write_openmp_by_elem

  implicit none

  !  1. Interface variables 

  integer(kind=di), intent(in)                            :: n
  real(kind=dr), allocatable, dimension(:), intent(inout) :: w       
  real(kind=dr), external                                 :: vector_function
  character(len=len_a), intent(in)                        :: file_system
  character(len=len_a), intent(in)                        :: file_name
  character(len=len_a), intent(in)                        :: type_of_build
  character(len=len_a), intent(in)                        :: num_files
  integer(kind=si), intent(in)                            :: trials
  character(len=len_a), intent(in)                        :: output_yes_no
  character(len=len_a), intent(in)                        :: stat_open
  character(len=len_a), intent(in)                        :: stat_close

  !  2. Local variables and parameters

  integer(kind=si)            :: trials_a
  integer(kind=si)            :: trials_b
  integer(kind=si)            :: i
  real(kind=dr)               :: t1
  real(kind=dr)               :: t2
  real(kind=dr)               :: t1_step
  integer(kind=si), parameter :: unit_a = 500_si
  integer(kind=si)            :: sys_t1
  integer(kind=si)            :: sys_t2
  integer(kind=si)            :: sys_t1_step
  integer(kind=si)            :: count_rate
  integer(kind=si)            :: count_max
  real(kind=dr)               :: sys_tmp

  !  3. Choose way to build the matrix 

  if (type_of_build == TYPE_OF_BUILD_FUNCTION) then
  
    trials_b = trials
    trials_a = 0_si
     
  else if (type_of_build == TYPE_OF_BUILD_VECTOR) then

    trials_b = 0_si
    trials_a = trials
  
  end if

  !  4. Write the vector - load the vector in RAM

  do i = 1, trials_a

    !  4-1. t1
    
    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t1_step)
      call system_clock(sys_t1_step, count_rate, count_max)
      if (i == 1_si) then
        call cpu_time(t1)
        call system_clock(sys_t1, count_rate, count_max)
      end if
    end if

    !  4-2. Allocate the vector

    allocate(w(1:n))

    !  4-3. Build the vector

    call vector_via_function(n,               & !  1.
                             w,               & !  3.
                             vector_function)   !  4.

    !  4-4. Write the vector

    call vector_write_openmp_all(n,           & !  1.
                                 w,           & !  2.
                                 file_system, & !  3.
                                 file_name,   & !  4.
                                 num_files,   & !  5.
                                 stat_open,   & !  6.
                                 stat_close)    !  7.

    !  4-5. Deallocate the vector

    deallocate(w)

    !  4-6. Some counter to indicate the progress  

    if (output_yes_no == OUTPUT_YES) then
      write(*,*) ""
      write(*,*) " 1 --> Counter Vector Write -------------------->", i
    end if

    !  4-7. t2 and total time

    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t2)
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1_step)*1.0_dr/count_rate
      write(*,*) ""
      write(*,*) " 2 --> Total CPU time for one write is:"
      write(*,*) "   --> ",t2-t1_step, " seconds."
      write(*,*) " 3 --> Total Wall-Clock time for one write is:"
      write(*,*) "   --> ",sys_tmp, " seconds."
      write(*,*) " 4 --> The write speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1_step)*60_si,&
                 " GBytes per CPU minute."
      write(*,*) " 5 --> The write speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1_step),&
                 " MBytes per CPU second."
      write(*,*) " 6 --> The write speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                 " GBytes per Wall-Clock minute."
      write(*,*) " 7 --> The write speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                 " MBytes per Wall-Clock second."

      write(*,*) ""
      if (i == trials_a) then
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1)*1.0_dr/count_rate
        call cpu_time(t2)
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
        write(*,*) " 8 --> Total CPU time for", trials_a," writes is:"
        write(*,*) "   --> ",t2-t1, " seconds."
        write(*,*) " 9 --> Total Wall-Clock time for", trials_a," writes is:"
        write(*,*) "   --> ",sys_tmp, " seconds."
        write(*,*) "10 --> Total amount of data written is:"
        write(*,*) "   --> ",trials_a*8*real(n,kind=dr)/1024.0_dr**3.0_dr," GBytes."
        write(*,*) "11 --> The average write speed is:"
        write(*,*) "   --> ",trials_a*8*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1)*60_si,&
                   " GBytes per CPU minute."
        write(*,*) "12 --> The average write speed is:"
        write(*,*) "   --> ",trials_a*8*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1),&
                   " MBytes per CPU second."
        write(*,*) "13 --> The average write speed is:"
        write(*,*) "   --> ",trials_a*8*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                   " GBytes per Wall-Clock minute."
        write(*,*) "14 --> The average write speed is:"
        write(*,*) "   --> ",trials_a*8*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                   " MBytes per Wall-Clock second."
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
      end if
    end if

  end do

  !  6. Write the vector - create element by element.

  do i = 1, trials_b

    !  6-1. t1
  
    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t1_step)
      call system_clock(sys_t1_step, count_rate, count_max)
      if (i == 1_si) then
        call cpu_time(t1)
        call system_clock(sys_t1, count_rate, count_max)
      end if
    end if

    !  6-2. Build and write the vector

    call vector_write_openmp_by_elem(n,               & !  1.
                                     vector_function, & !  2.
                                     file_system,     & !  3.
                                     file_name,       & !  4.
                                     num_files,       & !  5.
                                     stat_open,       & !  6.  
                                     stat_close)        !  7.

    !  6-3. Some counter to indicate the progress  

    if (output_yes_no == OUTPUT_YES) then
      write(*,*) ""
      write(*,*) " 1 --> Counter Vector Write -------------------->", i
    end if

    !  6-4. t2 and total time

    if (output_yes_no == OUTPUT_YES) then
      call cpu_time(t2)
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1_step)*1.0_dr/count_rate
      write(*,*) ""
      write(*,*) " 2 --> Total CPU time for one write is:"
      write(*,*) "   --> ",t2-t1_step, " seconds."
      write(*,*) " 3 --> Total Wall-Clock time for one write is:"
      write(*,*) "   --> ",sys_tmp, " seconds."
      write(*,*) " 4 --> The write speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1_step)*60_si,&
                 " GBytes per CPU minute."
      write(*,*) " 5 --> The write speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1_step),&
                 " MBytes per CPU second."
      write(*,*) " 6 --> The write speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                 " GBytes per Wall-Clock minute."
      write(*,*) " 7 --> The write speed is:"
      write(*,*) "   --> ",8*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                 " MBytes per Wall-Clock second."

      write(*,*) ""
      if (i == trials_b) then
      call system_clock(sys_t2, count_rate, count_max)
      sys_tmp = (sys_t2-sys_t1)*1.0_dr/count_rate
        call cpu_time(t2)
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
        write(*,*) " 8 --> Total CPU time for", trials_b," writes is:"
        write(*,*) "   --> ",t2-t1, " seconds."
        write(*,*) " 9 --> Total Wall-Clock time for", trials_b," writes is:"
        write(*,*) "   --> ",sys_tmp, " seconds."
        write(*,*) "10 --> Total amount of data written is:"
        write(*,*) "   --> ",trials_b*8*real(n,kind=dr)/1024.0_dr**3.0_dr," GBytes."
        write(*,*) "11 --> The average write speed is:"
        write(*,*) "   --> ",trials_b*8*real(n,kind=dr)/1024.0_dr**3.0_dr/(t2-t1)*60_si,&
                   " GBytes per CPU minute."
        write(*,*) "12 --> The average write speed is:"
        write(*,*) "   --> ",trials_b*8*real(n,kind=dr)/1024.0_dr**2.0_dr/(t2-t1),&
                   " MBytes per CPU second."
        write(*,*) "13 --> The average write speed is:"
        write(*,*) "   --> ",trials_b*8*real(n,kind=dr)/1024.0_dr**3.0_dr/sys_tmp*60_si,&
                   " GBytes per Wall-Clock minute."
        write(*,*) "14 --> The average write speed is:"
        write(*,*) "   --> ",trials_b*8*real(n,kind=dr)/1024.0_dr**2.0_dr/sys_tmp,&
                   " MBytes per Wall-Clock second."
        write(*,*) ""
        write(*,*) "==========================================================="
        write(*,*) ""
      end if
    end if

  end do

  end subroutine vector_write_sub

  end module m_5_write_subs

!======!
! FINI !
!======!
