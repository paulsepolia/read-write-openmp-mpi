#!/bin/bash

  # 1. compile

  mpic++.mpich2  -O3                          \
                 -Wall                        \
                 -std=c++98                   \
                 driver_read_write_mpi_v3.cpp \
                 -o x_gnu_mpich2
