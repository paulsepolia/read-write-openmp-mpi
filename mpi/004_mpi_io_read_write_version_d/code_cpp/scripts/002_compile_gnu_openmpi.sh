#!/bin/bash

  # 1. compile

  mpic++.openmpi  -O3                          \
                  -Wall                        \
                  -std=c++0x                   \
                  driver_read_write_mpi_v4.cpp \
                  -o x_gnu_openmpi
