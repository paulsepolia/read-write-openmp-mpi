#!/bin/bash
  
  #  1. compile

  sh 001_compile_intel.sh
  sh 002_compile_gnu_openmpi.sh
  sh 003_compile_gnu_mpich2.sh

  #  2. intel

  var1=4

  mpirun -n $var1 ./x_intel

  #  3. gnu-openmpi

  mpirun.openmpi -n $var1 ./x_gnu_openmpi

  #  4. gnu-mpich2

  mpirun.mpich2 -n $var1 ./x_gnu_mpich2
