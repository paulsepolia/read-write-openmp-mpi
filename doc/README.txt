
  1. This is a complete suite of OpenMP I/O and MPI I/O benchmarks.
  
  2. I have developed codes which can easily test any parallel file system
     and its I/O speed.

  3. You can open one single file and do I/O using all the MPI processes.

  4. You can open as many files as the number of MPI processes and do I/O.
     Each MPI process reads and writes to its own file.

  5. You can do something between the above limiting cases.
     You can open a specific number of files and chose a specific number of
     MPI processes to do I/O to them.

  6. I have tested the codes in my laptop using the
     Intel Cluster Studio 2012 and the GNU Fortran and C++ compilers
     in combination with OpenMPI and MPICH2.

  7. I have tested the codes on HECToR using the MPT 6.0 and the CRAY compiler.

  8. On HECToR using 1024 MPI processes I got back 30 GBytes per second
     read and write speed. 

  9. On Fujitsu using 80 MPI processes and 80 files I get back 2 GBytes I/O per second.
     On HECToR  using 80 MPI processes and 80 files I get back 2 GBytes I/O per second.
     The same I/O speed in both machines.

  #=======================#
  # Pavlos G. Galiatsatos #
  # 2013/09/19            #
  # Oxford, UK            #
  #=======================#
